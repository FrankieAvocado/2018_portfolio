var Track = function (name, url, buffer) {
	this.Name = name;
	this.TrackUrl = url
	this.BufferData = buffer
	

	this._settings = {};

	this.OnBeat = function (callback) {
		this._settings.OnBeat = callback || function (hzGroup, value) { };
	}

	this.SetBufferSource = function (bufferSource) {
		this._settings.Source = bufferSource;
	}

	this.SetGainNode = function (gainNode) {
		var $this = this;

		$this._settings.GainNode = gainNode;

		$this.Mute = function () {
			$this._settings.GainNode.gain.value = 0;
		}

		$this.UnMute = function () {
			$this._settings.GainNode.gain.value = 1;
		}
	}

	this.SetAnalyzer = function (analyzer) {
		var $this = this;

		$this._settings.Analyzer = analyzer;
	}

	this.Play = function (loop) {
		if (this._settings.Source) {
			this._settings.Source.loop = loop;
			this._settings.Source.start(0);
		}
	}
	
	this.Stop = function(){
		if(this._settings.Source)
		{
			this._settings.Source.stop();
		}
	}

	this._analyzeFrame = function (historyLength, beatThreshold, beatResolution) {

		var $this = this;
		$this._settings.History = $this._settings.History || [];

		if ($this._settings.History.length >= historyLength) {
			this._settings.History.splice(0, 1);
		}

		if ($this._settings.Analyzer) {

			var data = new Uint8Array(beatResolution);
			var numberOfBars = beatResolution;
			$this._settings.Analyzer.getByteFrequencyData(data);
			var chunkSize = Math.floor(data.length / numberOfBars);
			var beatList = [];

			for (var i = 0; i < numberOfBars; i++) {
				sum = 0;

				for (var j = 0; j < chunkSize; j++) {
					sum += data[(i * chunkSize) + j];
				}

				var avg = sum / chunkSize;

				beatList.push(avg);

				var historyTotal = 0;
				for (var k = 0; k < $this._settings.History.length; k++) {
					historyTotal += $this._settings.History[k].Averages[i];
				}

				var historyAvg = Math.floor(historyTotal / $this._settings.History.length);
				var valueToBeat = historyAvg * beatThreshold + historyAvg;
				if (avg > valueToBeat) {
					$this._settings.OnBeat(i, avg);
				}
			}

			$this._settings.History.push({
				Averages: beatList
			});


		}
	}
};

var Circle = function (xpos, ypos, startRadius, innerRadius, maxRadius, defaultHex) {
	this.X = xpos;
	this.Y = ypos;
	this.InnerRadius = innerRadius;
	this.StartRadius = startRadius;
	this.MaxRadius = maxRadius;
	this.Size = startRadius;

	this.SetSize = function (size) {
		var sizePercent = size / 255;
		var thisSize = Math.floor(sizePercent * this.MaxRadius);
		if (thisSize > this.StartRadius) {
			this.Size = thisSize;
		}
	}

	this.Shrink = function () {
		if (this.Size > this.StartRadius + 5) {
			this.Size -= 5;
		}
		else {
			this.Size = this.StartRadius;
		}
	}

	this.Draw = function (size) {
		var $this = this;
		var greyScale = (256 - $this.Size).toString(16);
		CanvasHooks.DrawCircle($this.X, $this.Y, $this.Size, "#" + greyScale + '00' + '00');
		//CanvasHooks.DrawCircle($this.X, $this.Y, $this.InnerRadius, "#FFFFFF");
	}
}

var Square = function (xpos, ypos, startRadius, innerRadius, maxRadius, defaultHex) {
	this.X = xpos;
	this.Y = ypos;
	this.InnerRadius = innerRadius;
	this.StartRadius = startRadius;
	this.MaxRadius = maxRadius;
	this.Size = startRadius;
	this.Amount = 0;
	this.TintR = 0;
	this.TintG = 0;
	this.TintB = 0;
	this.ShrinkSpeed = 5;

	this.SetSize = function (size) {
		this.Amount = size;
		var sizePercent = size / 255;
		var thisSize = Math.floor(sizePercent * this.MaxRadius);
		if (thisSize > this.StartRadius) {
			this.Size = thisSize;
		}
	}

	this.Shrink = function () {
		if (this.Amount > this.ShrinkSpeed) {
			this.Amount -= this.ShrinkSpeed;
		}
		else {
			this.Amount = 0;
		}
		
		this.SetSize(this.Amount);
	}

	this.Draw = function (size) {
		var $this = this;
		
		var scale = $this.Amount / 256;
		//var greySet = (scale >= 16 ?  "" : "0") + scale.toString(16);
		/*var rScale = (256 - $this.Amount + $this.TintR);
		var gScale = (256 - $this.Amount + $this.TintG);
		var bScale = (256 - $this.Amount + $this.TintB);
		
		rScale = rScale > 256 ? 256 : rScale;
		gScale = gScale > 256 ? 256 : gScale;
		bScale = bScale > 256 ? 256 : bScale;*/
		
		//var offsetSquare = Math.floor($this.MaxRadius / 2);
		CanvasHooks.DrawRect($this.X, $this.Y, $this.MaxRadius, $this.MaxRadius, {r: 234, g: 18, b: 18, a: scale});
		//CanvasHooks.DrawCircle($this.X, $this.Y, $this.InnerRadius, "#FFFFFF");
	}
	
	this.AddTint = function(r, g, b){
		this.TintR += (this.TintR > 32 || this.TintR < 0) ? 0 : r;
		this.TintG += (this.TintG > 32 || this.TintG < 0) ? 0 : g,
		this.TintB += (this.TintB > 32 || this.TintB < 0) ? 0 : b;
	}
}